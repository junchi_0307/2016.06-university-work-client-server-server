package java_HW_server;

import java.io.*;
import java.net.*;
import java.sql.*;

class ChatHandler extends Thread {

    private Socket socket;
    public BufferedReader in;
    public PrintWriter out;
    public ChatHandler me = this;
    static Room room[] = new Room[10];
    Room thisroom;
    String str;
    static int total = 0;
    int n;
    boolean chack, i, run_stop = true;
    String game[];
    String NAME;

    public ChatHandler(Socket socket) {
        this.socket = socket;
    }

    public ChatHandler() {

    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "BIG5"));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "BIG5"));
            boolean login_check = false;
            while (!login_check) {
                String user, pass;
                out.println("user:");
                out.flush();
                user = in.readLine();
                if (user.equals("/NEW")) {
                    New();
                } else {
                    out.println("pass:");
                    out.flush();
                    pass = in.readLine();

                    int r = DBlink.login(user, pass);

                    if (r == 1) {
                        out.println("login_success");
                        out.flush();
                        login_check = true;
                        NAME = user;
                    } else {
                        out.println("miss");
                        out.flush();
                        login_check = false;
                    }
                }
            }
            String num = in.readLine();
            int num_start = Integer.valueOf(num.substring(0, num.indexOf("-"))) - 1, num_end = Integer.valueOf(num.substring(num.indexOf("-") + 1)) - 1, tempnum = 0;
            for (int i = num_start; i < total && i < num_end; i++) {
                out.println(i + ". " + room[i].get_room_message());
                out.flush();
                tempnum++;
            }
            for (int i = tempnum; i < 8; i++) {
                out.println("無房間");
                out.flush();
            }
            chack = false;
            i = false;
            do {
                str = in.readLine();
                if (str.equals("無房間")) {
                } else if ('N' == str.charAt(0)) {//str=N-(人數)-(遊戲類型)
                    i = true;
                    game = str.split("-");
                    if (!game[0].equals("N")) {
                        i = false;
                    }
                } else {
                    chack = true;
                    n = Integer.valueOf(str);
                    if (n >= 0 && n < total) {
                        if (!(room[n].get_num() + 1 <= room[n].get_total())) {
                            chack = false;
                        }
                    } else {
                        chack = false;
                    }
                }
                if (!(chack || i)) {
                    out.println("not");
                    out.flush();
                }
            } while (!(chack || i));
            if (i) {
                room[total] = new Room(Integer.valueOf(game[1]), game[2], game[3]);
                thisroom = room[total];
                n = total;
                total++;

                thisroom.add(this, NAME);
                if (thisroom != null) {
                    out.println("link");
                    out.println(thisroom.get_room_name());
                    out.flush();
                }
                String NAMETT = "";
                for (int j = 0; j < thisroom.name.length; j++) {
                    out.println(thisroom.name[j]);
                }
                out.println("NAMETT_STOP");
                out.flush();
            } else {
                thisroom = room[n];

                thisroom.add(this, NAME);
                if (thisroom != null) {
                    out.println("link");
                    out.println(thisroom.get_room_name());
                    out.flush();
                }
                String NAMETT = "";
                for (int j = 0; j < thisroom.name.length; j++) {
                    out.println(thisroom.name[j]);
                }
                out.println("NAMETT_STOP");
                out.flush();
                for (int j = 0; j < thisroom.people.length; j++) {
                    if (thisroom.people[j] != this) {
                        for (int j2 = 0; j2 < thisroom.name.length; j2++) {
                            thisroom.people[j].out.println(thisroom.name[j2]);
                        }
                        thisroom.people[j].out.println("NAMETT_STOP");
                        thisroom.people[j].out.flush();
                    }
                }
            }
            while (!(in.ready() && in.readLine().equals("game"))) {
                Thread.currentThread().sleep(1000);
                if (thisroom.open) {
                    break;
                }
            }
            thisroom.open = true;
            out.println("game");
            out.flush();
            thisroom.game();
            in.close();
            out.close();
            socket.close();
        } catch (IOException ie) {

        } catch (SQLException ex) {
        } catch (InterruptedException ex) {
        }
    }

    private void New() throws IOException, SQLException {     //註冊
        boolean login_check = false;

        String user2 = "", pass2 = "";
        out.println("user:");
        out.flush();
        user2 = in.readLine();

        out.println("pass:");
        out.flush();
        pass2 = in.readLine();

        int r = DBlink.search(user2, pass2);
        if (r > 0) {
            out.println("新增成功");
            out.flush();
        } else {
            out.println("新增失敗");
            out.flush();
        }

        run();
    }

}
