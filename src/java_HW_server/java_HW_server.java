package java_HW_server;

import java.io.*;
import java.net.*;

public class java_HW_server {
    public static void main(String[] args) {
        try {
            ServerSocket s = new ServerSocket(12345);
            while (true) {
                Socket socket = s.accept();
                ChatHandler ChatHandler = new ChatHandler(socket);
                ChatHandler.start();
            }
        } catch (IOException ie) {
        }
    }
}
