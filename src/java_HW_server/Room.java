package java_HW_server;

import java.io.IOException;

public class Room {

    private int num;
    private int total;
    ChatHandler people[];
    boolean open;
    private String game;
    private String room_name;
    String name[];
    boolean end_all[] = new boolean[2];
    int end_num[] = new int[2];

    public Room(int total, String game, String room_name) {
        this.total = total;
        num = 0;
        people = new ChatHandler[total];
        name = new String[total];
        for (int i = 0; i < people.length; i++) {
            people[i] = new ChatHandler();
        }
        open = false;
        this.game = game;
        this.room_name = room_name;
        for (int i = 1; i < 53; i++) {
            math[i] = false;
        }
        for (int i = 0; i < 8; i++) {
            play1[i] = 0;
            play2[i] = 0;
        }
        for (int i = 0; i < end_all.length; i++) {
            end_all[i] = false;
        }
    }

    public void add(ChatHandler me, String NAME) {

        synchronized (people) {
            people[num] = me;
            name[num] = NAME;
            num++;
        }
    }

    public String get_room_message() {
        return room_name + "-" + num + "-" + total;
    }

    public int get_num() {
        return num;
    }

    public String get_room_name() {
        return room_name;
    }

    public int get_total() {
        return total;
    }

    int ab, a, b;

    public void game() {
        String temp = "";
        boolean tempB = false;
        boolean start = false;
        try {
            while (true) {
                for (int i = 0; i < people.length; i++) {
                    while (people[i].in.ready()) {
                        temp = people[i].in.readLine();
                        tempB = true;
                        ab = i;
                        break;
                    }
                    if (tempB) {
                        break;
                    }
                }
                if (tempB) {
                    if (temp.equals("start") && !start) {
                        start = true;
                        Start();
                    } else if (temp.equals("one")) {
                        one();
                    } else if (temp.equals("end")) {
                        end_all[ab] = true;
                        end_num[ab] = Integer.valueOf(people[ab].in.readLine());
                        if (end_all[0] && end_all[1]) {
                            End();
                        }
                    }
                    tempB = false;
                    ab = -1;
                }
            }
        } catch (IOException ex) {
        }
    }
    int play1[] = new int[8];
    int play2[] = new int[8];

    public void Start() {
        for (int i = 0; i < people.length; i++) {
            people[i].out.println("start");
        }
        play1[0] = rand();
        play1[1] = rand();
        play2[0] = rand();
        play2[1] = rand();
        people[0].out.println(play1[0]);
        people[0].out.println(play1[1]);
        people[0].out.println(play2[0]);
        people[0].out.println(play2[1]);
        people[0].out.flush();
        people[1].out.println(play2[0]);
        people[1].out.println(play2[1]);
        people[1].out.println(play1[0]);
        people[1].out.println(play1[1]);
        people[1].out.flush();
        a = 2;
        b = 2;
    }

    public void one() {
        people[0].out.println("one");
        people[1].out.println("one");
        for (int i = 0; i < 8; i++) {
            if (ab == 0 && play1[i] == 0) {
                play1[i] = rand();
                break;
            } else if (ab == 1 && play2[i] == 0) {
                play2[i] = rand();
                break;
            }
        }
        String mathtemp1 = "";
        String mathtemp2 = "";
        for (int i = 0; i < 8; i++) {
            mathtemp1 += play1[i] + "----";
            mathtemp2 += play2[i] + "----";
        }
        people[0].out.println(mathtemp1);
        people[0].out.println(mathtemp2);
        people[0].out.flush();
        people[1].out.println(mathtemp2);
        people[1].out.println(mathtemp1);
        people[1].out.flush();
    }

    public boolean math[] = new boolean[53];

    public int rand() {
        int i;
        do {
            i = (int) (Math.random() * 52 + 1);
        } while (math[i]);
        math[i] = true;
        return i;
    }

    public void End() {
        int numA = 0, numB = 0;

        people[0].out.println("end");
        people[1].out.println("end");

        String mathtemp1 = "";
        String mathtemp2 = "";
        for (int i = 0; i < 8; i++) {
            mathtemp1 += play1[i] + "----";
            mathtemp2 += play2[i] + "----";
            if (play1[i] != 0) {
                numA = i;
            }
            if (play2[i] != 0) {
                numB = i;
            }
        }
        people[0].out.println(mathtemp1);
        people[0].out.println(mathtemp2);
        people[0].out.println(end_num[1]);
        people[1].out.println(mathtemp2);
        people[1].out.println(mathtemp1);
        people[1].out.println(end_num[0]);

        if (numA >= 5 && numB >= 5 && end_num[0] <= 21 && end_num[1] <= 21) {
            people[0].out.println("平手");
            people[1].out.println("平手");
        } else if (numA >= 5 && end_num[0] <= 21) {
            people[0].out.println("恭喜" + name[0] + "是贏家");
            people[1].out.println("恭喜" + name[0] + "是贏家");
        } else if (numB >= 5 && end_num[1] <= 21) {
            people[0].out.println("恭喜" + name[1] + "是贏家");
            people[1].out.println("恭喜" + name[1] + "是贏家");
        } else if (end_num[0] == 21 && end_num[0] == 21) {
            people[0].out.println("平手");
            people[1].out.println("平手");
        } else if (end_num[0] == 21) {
            people[0].out.println("恭喜" + name[0] + "是贏家");
            people[1].out.println("恭喜" + name[0] + "是贏家");
        } else if (end_num[1] == 21) {
            people[0].out.println("恭喜" + name[1] + "是贏家");
            people[1].out.println("恭喜" + name[1] + "是贏家");
        } else if (end_num[0] > 21) {
            people[0].out.println("恭喜" + name[1] + "是贏家");
            people[1].out.println("恭喜" + name[1] + "是贏家");
        } else if (end_num[1] > 21) {
            people[0].out.println("恭喜" + name[0] + "是贏家");
            people[1].out.println("恭喜" + name[0] + "是贏家");
        } else if (end_num[0] > end_num[1]) {
            people[0].out.println("恭喜" + name[0] + "是贏家");
            people[1].out.println("恭喜" + name[0] + "是贏家");
        } else if (end_num[0] < end_num[1]) {
            people[0].out.println("恭喜" + name[1] + "是贏家");
            people[1].out.println("恭喜" + name[1] + "是贏家");
        }
        people[0].out.flush();
        people[1].out.flush();
    }
}
